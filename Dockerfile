FROM php:7.4-apache

RUN apt-get update && apt-get install -y \
    git \
    unzip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN "date"

RUN docker-php-ext-install pdo pdo_mysql

COPY conf/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY conf/apache2.conf /etc/apache2/apache2.conf
RUN a2enmod rewrite

COPY ./var /app
WORKDIR /app
RUN composer install