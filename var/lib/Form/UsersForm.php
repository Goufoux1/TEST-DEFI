<?php

namespace Form;

use Core\Form;

class UsersForm extends Form
{
    const data = [
        'name' => [
            'required' => true,
            'length' => [3, 80]
        ],
        'first_name' => [
            'required' => true,
            'length' => [3, 80]
        ],
        'email' => [
            'required' => true,
            'length' => [10, 165]
        ],
        'password' => [
            'required' => true,
            'length' => [10, 50000000]
        ]
    ];
    
    public function verif(array $data)
    {
        $this->requiredControl(self::data, $data);
        $this->launch(self::data, $data);
    }
}
