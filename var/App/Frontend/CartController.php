<?php

namespace App\Frontend;

use Core\AbstractController;
use Entity\Product;
use Form\ConnectForm;
use Module\Mail;

class CartController extends AbstractController
{
    public function view()
    {
        $cart = $this->request->getCart();
        $products = array();
        $form = new ConnectForm();

        if (is_array($cart)) {
            foreach($cart as $item) {
                
                /** @var Product $product */
                $product = $this->manager->findOneBy('product', ['WHERE' => 'id = ' . $item["id"]]);

                $productGalleriesFlags = [
                        'LEFT JOIN' => [
                            'table' => 'image',
                            'sndTable' => 'productImage',
                            'firstTag' => 'id',
                            'sndTag' => 'image'
                        ]
                    ];

                $productGalleries = $this->manager->findBy('productImage', ['WHERE' => "product = {$product->getId()}"], $productGalleriesFlags);

                if (null !== $product && false !== $product) {
                    array_push($products, ["id" => $item["id"], "name" => $product->getName(), "qty" => $item["qty"], "img" => $productGalleries]);
                }
            }
        }

        if ($this->request->hasPost()) {
            $data = $this->request->getAllPost();
            $form->verif($data);
            if (true === $form->isValid()) {
                $mail = new Mail();
                $content = '<h4>Mon super panier</h4><table style="border: 1px solid black;  border-collapse: collapse;">
                        <thead>
                            <th style="border: 1px solid black; padding: 5px;" >Nom</th>
                            <th style="border: 1px solid black; padding: 5px;" >Quantité</th>
                        </thead>
                        <tbody>';
                foreach($products as $item) {
                    $content .= '<tr>
                            <td style="border: 1px solid black; padding: 5px;" >' . $item["name"] . '</td>
                            <td style="border: 1px solid black; padding: 5px;" >' . $item["qty"] . '</td>
                            </tr>';
                }

                $content.='</tbody></table>';
                $mail->send("Regarde ce panier !", $content, $data["email"]);
                $this->notifications->addSuccess('Connexion réussie', 'Connexion réussie');
            }
        }


        return $this->render([
            'products' => $products,
        ]);
    }

    public function remove()
    {
        $productId = $this->app->routeur()->getBag("slug");
        $this->request->removeCart(intval($productId));
        $this->notifications->addSuccess('Produit supprimé du panier', 'Le produit a été correctement supprimé du panier .');
        return $this->response->referer();
    }

    public function removeall()
    {
        $this->request->removeAllCart();
        $this->notifications->addSuccess('Tous les produit ont été supprimé du panier', 'Tous les produit ont été correctement supprimé du panier .');
        return $this->response->referer();
    }

    public function add()
    {
        $productId = $this->app->routeur()->getBag("slug");
        $this->request->addCart(intval($productId), 1);
        $this->notifications->addSuccess('Produit supprimé du panier', 'Le produit a été correctement supprimé du panier .');
        return $this->response->referer();
    }
}
