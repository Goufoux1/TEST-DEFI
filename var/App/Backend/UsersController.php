<?php

namespace App\Backend;

use Core\AbstractController;
use Form\UsersForm;
use Service\Helper;

class UsersController extends AbstractController
{
    public function index()
    {
        $users = $this->manager->fetchAll('user');

        return $this->render([
            'users' => $users
        ]);
    }

    public function update()
    {
        $userId = $this->app->routeur()->getBag('id');

        $user = $this->manager->findOneBy('user', ['WHERE' => 'id = ' . $userId]);

        $form = new UsersForm();

        if ($this->request->hasPost()) {
            $data = $this->request->getAllPost();

            $form->verif($data);

            if (false === $form->isValid()) {
                goto out;
            }

            $data['id'] = $userId;

            if (true !== $this->manager->update('user', $data)) {
                $this->notifications->default('500', 'Erreur insertion', $this->manager->getError(), 'danger', $this->isDev());
            }

            $this->notifications->addSuccess('Mise à jour réussie', 'Le contenu a bien été mis à jour.');

            return $this->response->referer();
        }

        out:

        return $this->render([
            'user' => $user,
            'form' => $form
        ]);
    }

    public function remove()
    {
        $userId = $this->app->routeur()->getBag('id');

        if ($userId !== $this->app->user()->getId()) {
            $this->manager->remove('user', 'id', $userId);
        }
        $this->notifications->addSuccess('Suppression réussie', 'Le contenu a bien été supprimé.');

        return $this->response->referer();
    }

    public function new()
    {
        $form = new UsersForm();

        if ($this->request->hasPost()) {
            $data = $this->request->getAllPost();

            $form->verif($data);

            if (false === $form->isValid()) {
                goto out;
            }

            // print_r($data);
            if (true !== $this->manager->add('user', $data)) {
                $this->notifications->default('500', 'Erreur insertion', $this->manager->getError(), 'danger', $this->isDev());
            }

            $this->notifications->addSuccess('Création réussie', 'Le contenu a bien été ajouté.');

            return $this->response->referer();

        }

        out:

        return $this->render([
            'form' => $form
        ]);
    }
}
