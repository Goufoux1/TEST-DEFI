# Test Defi !

Mise en place d'un docker compose basé sur les images php, mysql et phpmyadmin, renseignement des Ids smtp dans l'env du docker

Initilisation du sql au lancement du container

- Composer install à faire dans le dossier var ainsi qu'un chmod 755
 
### Gestion Utilisateur :
- "Le client à besoin d'une gestion des utilisateurs pour gérer l'accès des rédacteurs par catégorie (produits / actualités...)"

Gestionnaire d'utilisateur très basique avec nom, prenom, email, pass sans prise en compte des Auth d'accés

![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/6.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/7.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/8.png)

### Informations Produits :
- "Le client à besoin de renseigner d'autres informations pour ces produits : 
-- Prix HT, Prix TTC, Prix d'achat, coût d'envoi, temps de préparation 
-- Ces informations doivent être visible par le client"

Ajout de 5 champs en BDD et dans le formaulaire, ainsi que dans la vue du produit
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/10.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/9.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/11.png)

### Panier :
- "Le client souhaite que les visiteurs puissent créer et envoyer un panier"

Ajout dans le panier un par un et suppression individuelle ou globale du panier avec possibilité d'un envoi du panier par mail
Panier stocké dans la session

![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/2.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/1.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/3.png)
![](https://github.com/DavidL54/TEST-DEFI/blob/main/docimg/4.png)
